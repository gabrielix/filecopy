scopy: Implementation of copy with sendfile()

fcopy: Implementation using ANSI/ISO C

ucopy: Implementation using Unix I/O

bcopy: Byte copy using standard streams
