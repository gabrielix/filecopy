#include <stdio.h>

int main(int argc, char *argv[])
{
  FILE *src = fopen("source", "r");

  FILE *dst = fopen("destiny", "w");

     while (!feof(src)) {
      int c;
      c=fgetc(src);
      if (c != EOF) fputc(c, dst);      
    }
  fclose(src);
  fclose(dst);
  return 0;
}
