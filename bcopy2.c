#include <stdio.h>

int main(int argc, char *argv[])
{
  FILE *src = fopen("source", "r");

  FILE *dst = fopen("destiny", "w");

  int c;
  while ((c=fgetc(src)) != EOF) {
    fputc(c, dst);
  }
  fclose(src);
  fclose(dst);
  return 0;
}
