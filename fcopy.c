#include <stdio.h>
#include <stdlib.h>

#define BLOCKSIZE 4*1024*1024

long ffilesize(FILE *f) {
  long origin = ftell(f);
  fseek(f, 0, SEEK_END);
  long ret = ftell(f);
  fseek(f, origin, SEEK_SET);
  return ret;
}
int main(int argc, char *argv[])
{
  FILE *src = fopen("source", "r");
  FILE *dst = fopen("destiny", "w");

  char *region=malloc(BLOCKSIZE); // 4 MB

  long totalsize = ffilesize(src);
  if(BLOCKSIZE >= totalsize) {
    fread(region, 1, totalsize, src);
    fwrite(region, 1, totalsize, dst);
  } else {
    while(totalsize > BLOCKSIZE) {
      fread(region, 1, BLOCKSIZE, src);
      fwrite(region, 1, BLOCKSIZE, dst);
      totalsize-=BLOCKSIZE;
    }
    fread(region, 1, totalsize, src);
    fwrite(region, 1, totalsize, dst);
  }

  fclose(dst);
  fclose(src);
  free(region);
  return 0;
}
