#include <sys/sendfile.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

size_t filesize(int fd) {
  off_t old_position = lseek(fd, 0, SEEK_CUR);
  off_t total_size = lseek(fd, 0, SEEK_END);
  lseek(fd, old_position, SEEK_SET);
  printf("Total size: %ld\n", total_size);
  return total_size;
}

int main(int argc, char *argv[])
{

  int src = open("source", O_RDONLY);
  int dst = open("destiny", O_RDWR | O_CREAT, 0644);

  if (src == -1) { puts("Source not opened"); return -1; }
  if (dst == -1) { puts("Destiny not opened"); return -2; }

  sendfile(dst, src, NULL, filesize(src));
  close(dst);
  close(src);
  
  return 0;
}
