#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

#define BUFFER_SIZE 4*1024*1024
#define BLOCK_SIZE 4*1024

size_t filesize(int fd) {
  size_t total;
  size_t origin = lseek(fd, 0, SEEK_CUR);
  total = lseek(fd, 0, SEEK_END);
  lseek(fd, origin, SEEK_SET);
  return total;
}

int main(int argc, char *argv[])
{
  struct stat statbuf;
  int src = open("source", O_RDONLY);
  fstat(src, &statbuf);  
  int dst = open("destiny", O_WRONLY|O_CREAT|O_TRUNC, statbuf.st_mode);


  void *region = malloc(BUFFER_SIZE);

  size_t total_size = filesize(src);

  if (total_size <= BUFFER_SIZE) {
    read(src, region, total_size);
    write(dst, region, total_size);
  }else {
    while (total_size > BUFFER_SIZE) {
      read(src, region, BUFFER_SIZE);
      write(dst, region, BUFFER_SIZE);
      total_size-=BUFFER_SIZE;
    }
    read(src, region, total_size);
    write(dst, region, total_size);
  }
  close(src);
  close(dst);
  free(region);
  return 0;
}
